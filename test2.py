import matplotlib as mpl

import numpy as np

from matplotlib import pyplot as plt
mpl.rcParams["text.usetex"] = True
# mpl.rcParams["mathtext.fontset"] = "stix"
fig, ax = plt.subplots()

ax.text(0.0, 0.0,'$\hat { N } _ { 3 } = \sum \sp f _ { j = 1 } a _ { j } \sp { \dagger } a _ { j } \, .$')
ax.axis('off')

plt.show()