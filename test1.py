import os
import ast
import json
import torch
import random
import shutil


def write_json(json_path, arr):
    with open(json_path, "w") as f:
        json.dump(arr, f)

def read_json(json_path):
    with open(json_path, "r") as f:
        arr = json.loads(f.read())
    return arr

def create_vocab():
    train = read_json("data/train.json")
    val = read_json("data/val.json")


    id2token = {0:'[START]', 1:'[END]', 2: '[NULL]'}
    token2id = {'[START]':0, '[END]':1, '[NULL]': 2}
    cnt = len(id2token)
    for i in val:
        # if config.debug and i > 500: break
        form = val[i]
        form = form.split()[:80]

        # build vocab
        for token in form:
            if token not in token2id:
                token2id[token] = cnt
                id2token[cnt] = token
                cnt += 1

    for i in train:
        # if config.debug and i > 500: break
        form = train[i]
        form = form.split()[:80]

        # build vocab
        for token in form:
            if token not in token2id:
                token2id[token] = cnt
                id2token[cnt] = token
                cnt += 1

    print(len(token2id))

    torch.save({
        'id2token' : id2token,
        'token2id' : token2id
    }, os.path.join('experiments/im2latex-draft/vocab.pkl'))
# txt = val["0"]
# for i in val:
#     break
def move_file():
    train = read_json("data/train.json")
    val = read_json("data/val.json")
    for i in train:
        shutil.copy("out/{}.png".format(i), "data/f_train")

    for i in val:
        shutil.copy("out/{}.png".format(i), "data/f_val")

def create_data():
    val = dict()
    train = dict()
    u = 0
    data = read_json("data.json")

    for i in data:
        if os.path.exists("out/{}.png".format(u)):
            check = random.choice([1, 2, 3, 4, 5, 6, 7])
            if check == 1:
                val[u] = data[i]["formula"]
            else:
                train[u] = data[i]["formula"]
        u += 1

    write_json("data/train.json", train)
    write_json("data/val.json", val)

    print(u)


if __name__ == '__main__':
    create_vocab()