import cv2
import numpy as np


kernel = np.ones((2,3),np.uint8)


def noise(noise_typ,image):
    if noise_typ == "gauss":
        row,col,ch= image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        return noisy
    elif noise_typ == "s&p":
        row,col,ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                for i in image.shape]
        out[tuple(coords)] = 1
        # Pepper mode
        num_pepper = np.ceil(amount* image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                for i in image.shape]
        out[tuple(coords)] = 0
        return out
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return noisy
    elif noise_typ =="speckle":
        row,col,ch = image.shape
        gauss = np.random.randn(row,col,ch)
        gauss = gauss.reshape(row,col,ch)
        noisy = image + image * gauss
        return noisy


def blur_image(im):
    # blur
    im = cv2.GaussianBlur(im,(5,5),0)
    return im


def black_and_white(image):
    # convert to black and white
    image = image.convert('1')
    return image


def black_and_white_adaptive_threshold(image):
    # convert to black and white
    image = np.array(image)
    image = image[:, :, ::-1].copy()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    background = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    return background


def erosion(im):
    im = cv2.erode(im, kernel, iterations=1)
    return im


def dilation(im):
    im = cv2.dilate(im,kernel,iterations = 1)
    return im


img = cv2.imread("output/1.png")
img = blur_image(img)
# noise = noise("gauss", img)
cv2.imshow("img", img)
cv2.waitKey(0)
cv2.destroyAllWindows()